# Computer Vision Exercises - RWTH Aachen

## Notebook link
- [Exercise 1 Notebook](./Exercise1/exercise_1.ipynb)

## Setting up the environment
```bash
$ conda env create -f env.yml
$ conda activate cv_exercise
```
